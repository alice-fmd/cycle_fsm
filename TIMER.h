// -*- mode: C++ -*-
#ifndef TIMER_H
#define TIMER_H
#include <dim/dic.hxx>
#include <smixx/smirtl.hxx>
#include <string>
#include <sstream>
#include <algorithm>
#include <pthread.h>

void Info(const char* msg, ...); 
void Error(const char* msg, ...);
void Fatal(const char* msg, ...);
void* timer(void* obj);

//____________________________________________________________________
struct TIMER : public SmiProxy
{
  friend void* timer(void* obj);
  /** Stat type */
  enum State_t { 
    /** Idle state */
    Idle = 1, 
    /** Running state */
    Running, 
    /** timeout state */
    Timeout
  };
    
  /** 
   * Constructor 
   * 
   * @param timeout Timeout in miliseconds
   */
  TIMER(unsigned int timeout=5, const std::string& domain=std::string("FMD_DCS"))
    : SmiProxy(const_cast<char*>(ObjectName())), 
      _timeout(timeout),
      _stopped(false)
  {
    attach(const_cast<char*>(domain.c_str()), 
	   const_cast<char*>(ObjectName()));
    setVolatile();
    SetState(Idle);
  }
  /** 
   * Get the object name
   * 
   * 
   * @return The object name 
   */
  const char* ObjectName() const { return "TIMER"; }
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  const char* State2Text(State_t s) const
  {
    switch (s) {
    case Idle:        return "IDLE";
    case Running:     return "RUNNING";
    case Timeout:     return "TIMEOUT";
    default:          return "IDLE";
    }
    return "IDLE";
  }
  /** 
   * Get the state as a text string
   * 
   * @return 
   */
  State_t State()
  {
    std::string s(getState());
    if      (s == "IDLE")    return Idle;
    else if (s == "RUNNING") return Running;
    else if (s == "TIMEOUT") return Timeout;
    return Idle;
  }
  /** 
   * Set the state from enumeration value
   * 
   * @param s New state
   */  
  void SetState(State_t s) 
  {
    Info("TIMER: Setting state to %s", State2Text(s));
    setState(const_cast<char*>(State2Text(s)));
  }
  /** 
   * Handle an SMI command (action) from state machine
   * 
   */
  virtual void smiCommandHandler()
  {
    Info("TIMER: Got action %s in state %s", getAction(), getState());
    const char* acts[] = { "START", "STOP", "RESET" };
    if      (testAction(const_cast<char*>(acts[0])))  Start();
    else if (testAction(const_cast<char*>(acts[1])))  Stop();
    else if (testAction(const_cast<char*>(acts[2])))  Reset();
  }
  /** 
   * Handle the start action
   * 
   */  
  void Start()
  {
    // Ignore start commands when running 
    if (State() == Running) { 
      SetState(State());
      return;
    }
    
    // Set the state to running 
    SetState(Running);
    _stopped = false;
    
    // Sleep for specified number miliseconds
    Info("TIMER: Starting thread");
    pthread_create(&_thread, 0, &timer, this);
  }
  /** 
   * Handle the stop action
   * 
   */  
  void Stop()
  {
    // Ignore start commands when not running 
    // if (State() != Running) { 
    // SetState(State());
    // return;
    // }
    
    // Set state to idle
    _stopped = true;
    SetState(Idle);
  }
  /** 
   * Handle the reset action
   * 
   */
  void Reset()
  {
    // Ignore start commands when not running 
    if (State() != Timeout) { 
      SetState(State());
      return;
    }
    
    // Set state to idle
    _stopped = true;
    SetState(Idle);
  }
  /** 
   * Whether to be verbose or not
   * 
   * @param verb 
   */  
  void SetVerbose(bool verb) 
  {
    if (verb) setPrintOn();
    else      setPrintOff();
  }
  /** 
   * Run timer in separate thread
   * 
   */
  void Run()
  {
    unsigned int i = 0;
    time_t start = time(NULL);
    Info("Starting timer (%d ms) on %s", _timeout, asctime(localtime(&start)));
    while (i < _timeout && !_stopped) { 
      dtq_sleep(1); // usleep(100);
      i++;
    }
    time_t stop = time(NULL);
    Info("Stopping timer (after %d s) on %s", stop - start, 
	 asctime(localtime(&stop)));

    // Set state to timeout. 
    State_t s = !_stopped ? Timeout : Idle;
    Info("TIMER: After %d s: %s", i, State2Text(s));
    SetState(s);
  }
  
  /** The timeout - in miliseconds */
  unsigned int _timeout;
  /** Stopped */
  bool _stopped;
  /** Our thread */ 
  pthread_t _thread;
};

void* timer(void* obj)
{
  if (!obj) return 0;
  TIMER* t = static_cast<TIMER*>(obj);
  t->Run();
  return 0;
}

#endif
//____________________________________________________________________
//
// EOF
//

  
