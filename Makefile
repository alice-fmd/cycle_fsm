PACKAGE		= cycle_fsm
VERSION		= 0.2
CXX		= g++ 
CXXFLAGS	= -c 
CPPFLAGS	= -I/usr/include/dim -I/usr/include/smixx -I.
LD		= g++ 
LDFLAGS		= -ldim -lsmi -pthread

DOMAIN		= FMD_DCS
OBJECT		= FMD_DCS

%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%.smi:%.smipp
	sed -e 's/@DOMAIN@/$(DOMAIN)/g' \
	    -e 's/@OBJECT@/$(OBJECT)/g' \
	    < $< > $@

%.sobj:%.smi
	smiTrans $< 

all:	timer cycle.sobj 

timer:timer.o
	$(LD) $(LDFLAGS) $< -o $@ 

clean:
	rm -rf *~ *.o *.sobj timer $(PACKAGE)-$(VERSION)* *.smi

test:	all
	./testCycle 

dist:
	mkdir -p $(PACKAGE)-$(VERSION)
	cp cycle.smipp cycle.config Makefile Options.h timer.cxx TIMER.h \
		testCycle $(PACKAGE)-$(VERSION)
	tar -czvf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	rm -rf 	$(PACKAGE)-$(VERSION)
