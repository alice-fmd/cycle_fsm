#include <iostream>
#include <TIMER.h>
#include <Options.h>
#include <cstdarg>
#include <cstdio>

//____________________________________________________________________
void Info(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  char buf[1024];
  vsnprintf(buf, 1024, msg, ap);
  std::cout << "TIMER: " << buf << std::endl;
  va_end(ap);
}

//____________________________________________________________________
int
main(int argc, char** argv)
{
  Option<bool>         hOpt('h', "help",    "Show this help", false,false);
  Option<bool>         VOpt('V', "version", "Show version number",false,false);
  Option<bool>         vOpt('v', "verbose", "Be verbose", false, false);
  Option<unsigned>     tOpt('t', "timeout", "Timeout in secs", 4);
  Option<std::string>  dOpt('d', "dns",     "DNS host", "localhost");
  Option<std::string>  DOpt('D', "domain",  "SMI Domain", "FMD_DCS");
  
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(DOpt);
  cl.Add(tOpt);
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) { 
    cl.Help();
    return 0;
  }
  
  std::cout << "DIM DNS node: " << dOpt.Value() << std::endl;
  DimClient::setDnsNode(dOpt.Value().c_str());
  DimServer::setDnsNode(dOpt.Value().c_str());

  TIMER           t(tOpt.Value(), DOpt.Value());

  t.SetVerbose(vOpt.Value());
  while (true) pause();

  return 0;
}
//____________________________________________________________________
//
// EOF
//
  
